package com.example;

import org.junit.Assert;
import org.junit.Test;
import ru.yandex.qatools.allure.annotations.Description;
import ru.yandex.qatools.allure.annotations.Title;

/**
 * Created by vampi_000 on 20.09.2015.
 */
public class Tests {

    @Title("This is first test")
    @Description("True answer")
    @Test
    public void test0 () {
        String str = ReadFile.readFile(0);
            String[] datas =  str.split(";");
        Assert.assertEquals(Integer.parseInt(datas[3]), sum(datas));
    }
    @Title("This is second test")
    @Description("True answer")
    @Test
    public void test1 () {
        String str = ReadFile.readFile(1);
        String[] datas =  str.split(";");
        Assert.assertEquals(Integer.parseInt(datas[3]), sum(datas));
    }
    @Title("This is third test")
    @Description("True answer")
    @Test
    public void test2 () {
        String str = ReadFile.readFile(2);
        String[] datas =  str.split(";");
        Assert.assertEquals(Integer.parseInt(datas[3]), sum(datas));
    }
    @Title("This is fourth test")
    @Description("False answer")
    @Test
    public void test3 () {
        String str = ReadFile.readFile(3);
        String[] datas =  str.split(";");
        Assert.assertEquals(Integer.parseInt(datas[3]), sum(datas));
    }



    public int sum (String[] data) {
        int val1 = Integer.parseInt(data[0]);
        int val2 = Integer.parseInt(data[1]);
        int sum = 0;

        if (data[2].equals("-")){
            sum = val1 - val2;
        } else if (data[2].equals("+")){
            sum = val1 + val2;
        } else if (data[2].equals("*")){
            sum = val1 * val2;
        } else if (data[2].equals("/")){
            sum = val1 / val2;
        }



        return sum;
    }
}