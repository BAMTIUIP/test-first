package com.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by vampi_000 on 20.09.2015.
 */
public class ReadFile {


        public static String readFile (int string) {
            String result = "";
            try {

                BufferedReader br = new BufferedReader(new FileReader(new File("src\\test\\data.txt")));
                String sCurrentLine;

                int i = 0;
                while ((sCurrentLine = br.readLine()) != null) {

                    if (i == string) {
                        result = sCurrentLine;
                    }
                    i++;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }


}
